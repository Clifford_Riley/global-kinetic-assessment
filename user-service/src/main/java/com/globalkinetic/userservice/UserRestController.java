/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalkinetic.userservice;

import com.globalkinetic.userservice.dto.ListUserDto;
import com.globalkinetic.userservice.dto.LoginDto;
import com.globalkinetic.userservice.dto.LoginResponseDto;
import com.globalkinetic.userservice.dto.LogoutRequestDto;
import com.globalkinetic.userservice.dto.NewUserDto;
import com.globalkinetic.userservice.exception.LoginFailedException;
import com.globalkinetic.userservice.exception.LogoutFailedException;
import com.globalkinetic.userservice.exception.UserAlreadyExistException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Clifford Riley
 */
@RestController
@RequestMapping("/api")
public class UserRestController {

    private final UserRepository userRepository;
    private final SimpMessagingTemplate template;

    @Autowired
    public UserRestController(UserRepository userRepository,
            SimpMessagingTemplate template) {
        this.userRepository = userRepository;
        this.template = template;
    }

    private void sendActiveUsers() {
        List<User> allUsers = this.userRepository.findAllActiveUsers();

        this.template.convertAndSend("/active", allUsers.size());
    }

    private void checkIfUserExist(String username) {
        this.userRepository.findByUsername(username)
                .ifPresent(user -> {
                    throw new UserAlreadyExistException(username);
                });
    }

    @RequestMapping(method = RequestMethod.POST, value = "/user/add")
    public ResponseEntity<Void> addUser(@RequestBody NewUserDto newUserDto) {
        checkIfUserExist(newUserDto.getUsername());

        User user = new User(newUserDto, newUserDto.getPassword());

        this.userRepository.save(user);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users")
    public ListUserDto getAllUsers() {
        List<User> allUsers = this.userRepository.findAll();

        return new ListUserDto(allUsers);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/activeusers")
    public ListUserDto getAllActiveUsers() {
        List<User> allUsers = this.userRepository.findAllActiveUsers();

        return new ListUserDto(allUsers);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/user/login")
    public LoginResponseDto performLogin(@RequestBody LoginDto loginDto) {

        Optional<User> userOp = this.userRepository.findByUsername(loginDto.getUsername());

        User user = userOp.orElseThrow(() -> new LoginFailedException());

        if (!user.getPassword().equals(loginDto.getPassword())) {
            throw new LoginFailedException();
        }

        String jwtToken = Jwts.builder().setSubject(loginDto.getUsername())
                .setIssuedAt(new Date()).claim("userId", user.getId() + "")
                .signWith(SignatureAlgorithm.HS256, "secret123")
                .compact();

        user.setToken(jwtToken);
        user.setLastActiveDate(new Date());

        this.userRepository.save(user);
        this.sendActiveUsers();

        return new LoginResponseDto(user.getId(), jwtToken);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/user/logout/{id}")
    public void performLogout(@PathVariable("id") Long userId,
            @RequestBody LogoutRequestDto logoutRequest) {
        Optional<User> userOp = this.userRepository.findById(userId);

        if (userOp.isPresent()) {
            User user = userOp.get();

            if (user.getToken().equals(logoutRequest.getToken())) {
                user.setToken("");
                user.setLastActiveDate(null);

                this.userRepository.save(user);

                sendActiveUsers();
            } else {
                throw new LogoutFailedException();
            }
        }
    }
}
