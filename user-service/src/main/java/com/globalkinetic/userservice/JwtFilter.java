/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalkinetic.userservice;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.RequiredTypeException;
import io.jsonwebtoken.SignatureException;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.filter.GenericFilterBean;

/**
 *
 * @author Clifford Riley
 */
public class JwtFilter extends GenericFilterBean {

    private UserRepository userRepository;

    private static final long ACTIVE_TIME = 1000 * 60 * 3;

    public JwtFilter(UserRepository userRepository) {
        super();
        this.userRepository = userRepository;
    }

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
            throws IOException, ServletException {

        final HttpServletRequest request = (HttpServletRequest) req;
        final HttpServletResponse response = (HttpServletResponse) res;
        final String authHeader = request.getHeader("authorization");

        if ("OPTIONS".equals(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);

            chain.doFilter(req, res);
        } else {
            if (authHeader == null || !authHeader.startsWith("Bearer ")) {
                throw new ServletException("auth-error");
            }

            final String token = authHeader.substring(7);

            try {
                final Claims claims = Jwts.parser().setSigningKey("secret123").parseClaimsJws(token).getBody();
                Long userId = new Long(claims.get("userId", String.class));

                Optional<User> userOp = this.userRepository.findById(userId);

                User user = userOp.orElseThrow(() -> new ServletException("auth-error"));

                if (user.getToken() == null || !user.getToken().equals(token)) {
                    throw new ServletException("auth-error");
                }

                if (user.getLastActiveDate() == null
                        || System.currentTimeMillis() - user.getLastActiveDate().getTime() > ACTIVE_TIME) {
                    throw new ServletException("auth-error");
                }

                user.setLastActiveDate(new Date());
                this.userRepository.save(user);
            } catch (final SignatureException | RequiredTypeException e) {
                throw new ServletException("auth-error");
            }

            chain.doFilter(req, res);
        }
    }
}
