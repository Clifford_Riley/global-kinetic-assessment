/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalkinetic.userservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Clifford Riley
 */
@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class UserAlreadyExistException extends RuntimeException {

    public UserAlreadyExistException(
            String userName) {
        super("user with 'username:" + userName + "' already exist.");
    }
}
