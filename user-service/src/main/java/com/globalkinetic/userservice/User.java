/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalkinetic.userservice;

import com.globalkinetic.userservice.dto.NewUserDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Clifford Riley
 */
@Entity
public class User {

    @Id
    @GeneratedValue
    private Long id;

    private String username;
    private String password;
    private String phone;

    private String token;

    private Date lastActiveDate;

    private User() {
    } // JPA only

    public User(String username, String password, String phone) {
        super();

        this.username = username;
        this.password = password;
        this.phone = phone;
    }

    public User(NewUserDto newUserDto, String encodedPassword) {
        super();

        this.username = newUserDto.getUsername();
        this.password = encodedPassword;
        this.phone = newUserDto.getPhone();
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getLastActiveDate() {
        return lastActiveDate;
    }

    public void setLastActiveDate(Date lastActiveDate) {
        this.lastActiveDate = lastActiveDate;
    }

}
