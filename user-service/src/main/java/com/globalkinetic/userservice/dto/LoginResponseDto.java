/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalkinetic.userservice.dto;

/**
 *
 * @author Cliford Riley
 */
public class LoginResponseDto {
    private Long id;
    private String token;
    
    private LoginResponseDto(){
        super();
    }
    
    public LoginResponseDto(Long id,String token){
        this.id = id;
        this.token = token;
    }

    public Long getId() {
        return id;
    }

    public String getToken() {
        return token;
    }
}
