/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalkinetic.userservice.dto;

import com.globalkinetic.userservice.User;

/**
 *
 * @author Clifford Riley
 */
public class SimpleUserDto {

    private final Long id;
    private final String username;
    private final String phone;

    public SimpleUserDto(User user) {
        super();

        this.id = user.getId();
        this.phone = user.getPhone();
        this.username = user.getUsername();
    }

    public Long getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public String getUsername() {
        return username;
    }
}
