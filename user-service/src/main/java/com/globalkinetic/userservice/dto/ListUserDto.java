/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalkinetic.userservice.dto;

import com.globalkinetic.userservice.User;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Clifford Riley
 */
public class ListUserDto {

    private final List<SimpleUserDto> users;

    public ListUserDto(List<User> users) {
        super();
        this.users = users.stream()
                .map(user -> new SimpleUserDto(user))
                .collect(Collectors.toList());
    }
        
    public List<SimpleUserDto> getUsers() {
        return users;
    }
}
