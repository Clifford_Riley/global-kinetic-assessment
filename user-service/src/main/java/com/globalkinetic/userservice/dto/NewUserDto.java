/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalkinetic.userservice.dto;

/**
 *
 * @author Clifford Riley@
 */
public class NewUserDto {
    private String username;
    private String password;
    private String phone;
    
    private NewUserDto(){
        super();
    }
    
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }
}
