/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalkinetic.userservice;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * This task run every minute. It used to cleanup any users that wasn't logout
 * properly and token expired.
 *
 * @author Clifford
 */
@Component
public class TokenCleanupTask {
//    private final Logger LOGGER = LoggerFactory.getLogger(TokenCleanupTask.class);

    private static final long ACTIVE_TIME = 1000 * 60 * 3;

    private final UserRepository userRepository;
    private final SimpMessagingTemplate template;

    @Autowired
    public TokenCleanupTask(UserRepository userRepository,
            SimpMessagingTemplate template) {
        super();
        this.userRepository = userRepository;
        this.template = template;
    }

    private void sendActiveUsers() {
        List<User> allUsers = this.userRepository.findAllActiveUsers();

        this.template.convertAndSend("/active", allUsers.size());
    }

    @Scheduled(fixedRate = 1000 * 20)
    public void checkUsers() {
        List<User> activeUsers = this.userRepository.findAllActiveUsers();

        AtomicBoolean updated = new AtomicBoolean(false);

        activeUsers.stream()
                .filter((user) -> System.currentTimeMillis() - user.getLastActiveDate().getTime() > ACTIVE_TIME)
                .forEach((user) -> {
                    user.setLastActiveDate(null);
                    user.setToken("");

                    this.userRepository.save(user);
                    updated.set(true);
                });

        if (updated.get()) {
            sendActiveUsers();
        }
    }
}
