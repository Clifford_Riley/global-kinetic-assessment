/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalkinetic.userservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.*;
import org.springframework.http.MediaType;
import org.springframework.mock.http.MockHttpOutputMessage;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

/**
 *
 * @author Clifford Riley
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserServiceApplication.class)
@WebAppConfiguration
public class UserRestControllerTest {

    private final List<User> userList = new ArrayList<>();

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserRepository userRepository;

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        this.userRepository.deleteAllInBatch();

        this.userList
                .add(this.userRepository
                        .save(new User("testUserOne", "passwordOne", "Phone1")));
        this.userList
                .add(this.userRepository
                        .save(new User("testUserTwo", "passwordTwo", "Phone2")));
    }

    @Test
    public void testAddUser_UserAlreadyExist() throws Exception {
        String userJson = json(new User("testUserOne", "anything", "aphone"));
        
        this.mockMvc.perform(post("/api/user/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson))
                .andExpect(status().isNotAcceptable());
    }
    
    @Test
    public void testAddUser() throws Exception {
        String userJson = json(new User("testUserThree", "anything", "aphone"));
        
        this.mockMvc.perform(post("/api/user/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson))
                .andExpect(status().isCreated());
        
        Optional<User> newUserOp = userRepository.findByUsername("testUserThree");
        
        assertTrue(newUserOp.isPresent());
        
        User newUser = newUserOp.get();
        assertEquals("User name not the same",newUser.getUsername(), "testUserThree");
        assertEquals("Password not the same",newUser.getPassword(), "anything");
        assertEquals("Phone not the same",newUser.getPhone(), "aphone");
    }

    @Test
    public void testGetAllUsers() throws Exception {
        mockMvc.perform(get("/api/users"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.users", hasSize(2)))
                .andExpect(jsonPath("$.users[0].id", is(this.userList.get(0).getId().intValue())))
                .andExpect(jsonPath("$.users[0].phone", is(this.userList.get(0).getPhone())))
                .andExpect(jsonPath("$.users[1].id", is(this.userList.get(1).getId().intValue())))
                .andExpect(jsonPath("$.users[1].phone", is(this.userList.get(1).getPhone())));
    }

}
