## Global Kinetic Assessment

To run the application go to the dist directory in root project folder and run following command

        java -jar user-service-0.0.1-SNAPSHOT.jar

**Note ** : Ensure that java is [installed](https://java.com/en/download/) and in path.

To access the system go to [http://localhost:8080/](http://localhost:8080/).

## Build clean jar.

Requires the followng to be installed.

1. Java - [install](https://java.com/en/download/) and add to path.
2. Ant - [install](https://ant.apache.org/) and add to path.
3. Maven - [install](https://maven.apache.org/) and add to path. 
4. Node.js - [install](https://nodejs.org/en/) and add to path. 
5. npm - [install](https://www.npmjs.com) 
5. Angular CLI - [install](https://cli.angular.io/)

To build new jar just rul following from project root directory.

        ant

This will generate a jar file in user-service\target.

## Run in development mode.

**user-service**

Requires the followng to be installed.

1. Java - [install](https://java.com/en/download/) and add to path.
2. Maven - [install](https://maven.apache.org/) and add to path.

This is a standard mvn project. It used spring boot. 
To start execute following command.

        mvn clean spring-boot:run

**user-web**

Requires the followng to be installed.

1. Node.js - [install](https://nodejs.org/en/) and add to path. 
2. npm - [install](https://www.npmjs.com) 
3. Angular CLI - [install](https://cli.angular.io/)

This is a angular project. 
To start execute following command.

        ng serve

To access the front end go to [http://localhost:4200/](http://localhost:4200/).