export class Message {
  message: String;
  messageType: String;
  messageClass:String;
}

export const MESSAGE_TYPE_INFO: String = "type_info";
export const MESSAGE_TYPE_WARNING: String = "type_warning";
export const MESSAGE_TYPE_ERROR: String = "type_error";
export const MESSAGE_TYPE_SUCCESS: String = "type_success";


