import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';

import { Message, MESSAGE_TYPE_ERROR, 
    MESSAGE_TYPE_INFO, MESSAGE_TYPE_WARNING, 
    MESSAGE_TYPE_SUCCESS } from './../data/message'

@Injectable()
export class MessageService {

  constructor() { }

  messages: Message[] = [];

  removeFirst():void {
    this.messages.shift();
  }

  display(message: Message): void {
    this.messages.push(message);

    let timer = Observable.timer(3000);
    timer.subscribe(t=>this.removeFirst());
  }

  displayInfo(message: String): void {
    let newMsg = new Message();
    newMsg.message = message;
    newMsg.messageType = MESSAGE_TYPE_INFO;
    newMsg.messageClass = "alert-info";

    this.display(newMsg);
  };

  displayWarning(message: String): void {
    let newMsg = new Message();
    newMsg.message = message;
    newMsg.messageType = MESSAGE_TYPE_WARNING;
    newMsg.messageClass = "alert-warning";

    this.display(newMsg);
  };

  displayError(message: String): void {
    let newMsg = new Message();
    newMsg.message = message;
    newMsg.messageType = MESSAGE_TYPE_ERROR;
    newMsg.messageClass = "alert-danger";

    this.display(newMsg);
  };

  displaySuccess(message: String): void {
    let newMsg = new Message();
    newMsg.message = message;
    newMsg.messageType = MESSAGE_TYPE_SUCCESS;
    newMsg.messageClass = "alert-success";

    this.display(newMsg);
  };
}
