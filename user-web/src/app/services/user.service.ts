import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';


import { NewUser } from './../data/new-user';
import { MessageService } from './message.service';
import { LoginResponse } from '../data/login-response';
import { SimpleUser } from '../data/simple-user';
import { UserList } from '../data/user-list';


import { environment } from './../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class UserService {

  private loginResponse: LoginResponse;
  private logoutTimer: Observable<any> = null;
  private subscription : any;


  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private router: Router
  ) {
  }

  private peformForceLogout(){
    console.log("FORCE OUT xxxxxxx");
    this.subscription.unsubscribe();
    this.performLogout().subscribe(
      () => this.router.navigate([""])
    ); 
  }

  private createTimer(){
    console.log("FORCE OUT");
    if(this.logoutTimer !== null){
      this.subscription.unsubscribe();
    }

    this.logoutTimer = Observable.timer(1000*60*3,10000);

    this.subscription = this.logoutTimer.subscribe(
      () => {
        this.peformForceLogout()
      });
  }
  
  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', errorReturn: T, result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      if (error.status === 0) {
        this.messageService.displayError("Couldn't connect to back end.Please consult helpdesk.");
      } else if (error.status === 500
        && error.error.message === 'auth-error') {
        this.messageService.displayError("User not authoriszed.Please try logging in.");
        this.router.navigate([""]);
      } else {
        this.messageService.displayError(error.error.message);
      }

      // Let the app keep running by returning an empty result.
      return of(errorReturn as T);
    };
  }

  addUser(newUser: NewUser): Observable<String> {
    const url = `${environment.coreUrl}/api/user/add`;
    return this.http.post<String>(url, newUser, httpOptions).pipe(
      catchError(this.handleError<String>('addUser', "ERROR"))
    ).map(results => {
      if (results !== "ERROR")
        return "DONE";
      else return results;
    });
  }

  performLogin(username: String, password: String): Observable<String> {
    const url = `${environment.coreUrl}/api/user/login`;
    var data = {
      username: username,
      password: password
    };

    return this.http.post<LoginResponse>(url, data, httpOptions).pipe(
      catchError(this.handleError<LoginResponse>('performlogin', null))
    ).map(results => {
      if (results !== null) {
        this.loginResponse = results;
        this.createTimer();
        return "DONE";
      } else {
        this.loginResponse = null;
        return "";
      }
    });
  }

  performLogout(): Observable<String> {
    if (this.loginResponse) {
      const url = `${environment.coreUrl}/api/user/logout/${this.loginResponse.id}`;
      var data = {
        token: this.loginResponse.token
      };

      return this.http.post<String>(url, data, httpOptions).pipe(
        catchError(this.handleError<String>('performLogout', "ERROR"))
      ).map(results => {
        if (results !== "ERROR") {
          this.loginResponse = null;
          return "DONE";
        }
        else return results;
      });
    }
  }

  findAllUser(): Observable<UserList> {
    const url = `${environment.coreUrl}/api/users`;
    var token: String = "";

    if (this.loginResponse) {
      token = this.loginResponse.token;
    }

    const authHttpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'authorization': `Bearer ${token}`
        }
      )
    };

    return this.http.get<UserList>(url, authHttpOptions).pipe(
      catchError(this.handleError<UserList>('userList', null))
    );
  }
}
