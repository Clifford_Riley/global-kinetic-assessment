import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserList } from '../data/user-list';
import { UserService } from '../services/user.service';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  userList: UserList = {
    users: []
  };

  constructor(
    private userService: UserService,
    private messageService: MessageService,
    private router: Router
  ) { }

  loadAllUsers(displayMessage: boolean): void {
    this.userService.findAllUser()
      .subscribe(result => {
        this.userList = result;
        if (!this.userList) {
          this.userList = {
            users: []
          };
        }

        if (displayMessage) {
          this.messageService.displayInfo("User list refreshed.");
        }
      });
  }

  ngOnInit() {
    this.loadAllUsers(false);
  }

  onUserLogout(): void {
    this.messageService.displaySuccess('User logged out.');
    this.router.navigate([""]);
  }
  
  performLogout(): void {
    this.userService.performLogout()
      .subscribe(() => this.onUserLogout());
  }
}
