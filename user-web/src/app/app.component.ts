import { Component, HostListener, Injectable } from '@angular/core';
import {
  trigger, state,
  style, animate,
  transition
} from '@angular/animations';

import { environment } from './../environments/environment';

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('flash', [
      state('*', style({ opacity: '1' })),
      transition('* => *', [
        style({  opacity: '0'}),
        animate(500)
      ])
    ]),
  ]
})
export class AppComponent {
  private stompClient;

  activeUsers: Number = 0;

  title = 'Global Kinetic User Mangement';

  constructor() {
    this.initializeWebSocketConnection();
  }

  initializeWebSocketConnection() {
    let ws = new SockJS(environment.webSocketUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe("/active", (message) => {
        console.log(message.body);
        if (message.body) {
          that.activeUsers = message.body;
        }
      });
    });
  }
}
