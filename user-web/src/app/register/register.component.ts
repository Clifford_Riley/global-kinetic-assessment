import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { UserService } from './../services/user.service';
import { MessageService } from './../services/message.service';

import { NewUser } from './../data/new-user'


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  newUser: NewUser = {
    username: "",
    password: "",
    phone: ""
  };

  busy:boolean=false;

  constructor(
    private router: Router,
    private userService: UserService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
  }

  onUserAddResponse(status): void {
    this.busy = false;
    if (status === "DONE") {
      this.router.navigate(['/']);

      this.messageService
        .displaySuccess("User : " + this.newUser.username + " registered.");
    } 
  }

  registerNewUser(): void {
    this.busy = true;
    this.userService
      .addUser(this.newUser)
      .subscribe(status => this.onUserAddResponse(status));
  }
}
