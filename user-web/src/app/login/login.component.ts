import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../services/user.service';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username:string = "";
  password:string = "";

  busy:boolean = false;

  constructor(
    private router: Router,
    private userService: UserService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
  }

  onLoginResponse(status): void {
    this.busy = false;
    if (status === "DONE") {
      this.router.navigate(['/userlist']);

      this.messageService
        .displaySuccess("User : " + this.username + " login successfully.");
    } 
  }

  performLogin():void {
    this.busy = true;
    this.userService
      .performLogin(this.username,this.password)
      .subscribe(status => this.onLoginResponse(status));
  }
}
