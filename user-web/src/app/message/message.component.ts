import { Component, OnInit } from '@angular/core';
import {
  trigger,state,
  style, animate,
  transition
}                            from '@angular/animations';

import { MessageService }    from './../services/message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css'],
  animations: [
    trigger('moveDown', [
      state('in', style({transform: 'translateY(0)'})),
      state('in', style({transform: 'translateY(100)'})),
      transition(':enter', [
        style({transform: 'translateY(-100%)'}),
        animate(500)
      ]),
      transition(':leave', [
        animate(500, style({transform: 'translateY(100%)'}))
      ]),
      transition('* => *', [
        style({transform: 'translateY(-100%)'}),
        animate(500)
      ])
    ]),
  ]
})
export class MessageComponent implements OnInit {

  constructor(public messageService: MessageService) { }

  ngOnInit() {

  }

}
